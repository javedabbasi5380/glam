
$(document).ready(function() {

  var star_rating_width = $('.fill-ratings span').width();
  $('.star-ratings').width(star_rating_width);
});

$(document).ready(function () {
  var readURL = function(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.profile-pic-22').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('click', 'someyourContainer .dropdown-menu', function (e) {
  e.stopPropagation();
});

$(".file-upload").on('change', function(){
    readURL(this);
});

$(".upload-button").on('click', function() {
   $(".file-upload").click();
});
  serviceslider();
});

// $(function(e) {
//     $('.review-nxt-btn').on("click", function() {
//         $(".reviews-slider-next").click();
//     });
// });
$(function (e) {
  $(".custom-header-link").on("click", function () {
    $(".custom-header-link").removeClass("custom-link-active");
    $(this).addClass("custom-link-active");
  });
});

$(function (e) {
  $(".custom-dropdown-ul li a").on("click", function () {
   var select = $(this).text();
   $( ".select-item-drop" ).text(select);
  });
})

$(function (e) {
  $(".custom-dropdown-checks").on("click", function (event) {
    event.stopPropagation();
  });
  
})

$(function (e) {
  $(".open-profile-modal").on("click", function (event) {
     $('#profile-modal').modal('show');
    event.preventDefault();
   
  });
});
$(function (e) {
  $(".close-location-modal").on("click", function (event) {
    event.preventDefault();
    $('#location-modal').modal('hide');
   
  });
});

$(function (e) {
  $(".open-locate-modal").on("click", function (event) {
    event.preventDefault();
    $('#profile-modal').modal('hide');
    $('#location-modal').modal('show');

   
  });
});


// upload file start
function previewFile() {
  var preview = document.querySelector("#profile-image1");
  var file = document.querySelector("input[type=file]").files[0];
  var reader = new FileReader();

  reader.addEventListener(
    "load",
    function () {
      preview.src = reader.result;
    },
    false
  );

  if (file) {
    reader.readAsDataURL(file);
  }
}
$(function () {
  $("#profile-image1").on("click", function () {
    $("#profile-image-upload").click();
  });
});
$(function () {
  $('.profile-upload-photo').change(function(e){
    var fileName = e.target.files[0].name;
    $( ".file-names-p" ).text(fileName);
});
});
// upload file end


    // functions
    function serviceslider() {
      $(".home-slider").slick({
          infinite: true,
          autoplay: false,
          autoplaySpeed: 2000,
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 1000,
          dots: true,
          arrows: true,
          prevArrow: "<img class='a-left control-c prev slick-prev home-arrow-slider home-arrow-left ' src='images/icons/left-arrow.png'/>",
          nextArrow: "<img class='a-right control-c next slick-next home-arrow-slider home-arrow-right' src='images/icons/right-arrow.png'/>",
          responsive: [{
                  breakpoint: 1200,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                  }
              },
              {
                  breakpoint: 992,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      arrows: false
                  }
              },
              {
                  breakpoint: 767,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      arrows: false
                  }
              }
          ]
  
      });
  
  }