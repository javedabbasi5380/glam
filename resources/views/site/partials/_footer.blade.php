<footer id="footer-com" >
    <div class="footer-classes">
        <div class="container">
            <div class="row">
                <div class=" col-lg-4 col-sm-12">
                    <div class="about-glam">
                        <h4 class="h-4-font text-white mb-3">ABOUT Glam</h4>
                        <p class="text-white">Glam is the online destination for beauty & wellness professionals and clients. Professionals can showcase their work, connect with new and existing clients, and build their business. Clients can discover new services and providers, book appointments online, and get inspired.</p>
                        <img src="{{asset('site/images/logo.png')}}" class="logo-footer" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="quick-link-footer">
                        <h4 class="h-4-font text-white mb-3">ABOUT Glam</h4>
                        <ul>
                            <li><a href="#">Glam Blog</a></li>
                            <li><a href="#">MEDIA</a></li>
                            <li><a href="#">Press</a></li>
                            <li><a href="#">TALK TO US</a></li>
                            <li><a href="#">info@glam.com</a></li>
                            <li><a href="#">Glam Help Center</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="quick-link-footer">
                        <h4 class="h-4-font text-white mb-3">Popular</h4>
                        <ul>
                            <li><a href="#">FIND PROFESSIONALS</a></li>
                            <li><a href="#">GET LISTED</a></li>
                            <li><a href="#">TEAM</a></li>
                            <li><a href="#">CAREERS</a></li>
                            <li><a href="#">TERMS FOR PROS</a></li>
                            <li><a href="#">TERMS FOR CLIENTS</a></li>
                            <li><a href="#">PRIVACY</a></li>
                            <li><a href="#">Press</a></li>
                            <li><a href="#">SITEMAP</a></li>
                        </ul>
                        <ul class="social-icons">
                            <li><a href="#" class="bg-info"><i class="fab  fa-instagram-square"></i></a></li>
                            <li><a href="#" class="bg-info"><i class="fab  fa-twitter"></i></a></li>
                            <li><a href="#" class="bg-info"><i class="fab  fa-facebook-f"></i></a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-footer container-fluid">
        <div class="row">
            <div class="col-md-12">
                <p class="text-white text-center mt-3">© 2021 Glam, Inc. All rights reserved</p>
            </div>
        </div>
    </div>
</footer>
