<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') | Glam</title>
    <link rel="stylesheet" href="{{asset('site/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/slick.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/slick-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/intl-tel-input.css')}}">
    <link rel="stylesheet" href="{{asset('site/css/style.css')}}">

    <script src="{{asset('site/js/jquery.min.js')}}"></script>
    <script src="{{asset('site/js/popper.min.js')}}"></script>
    <script src="{{asset('site/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('site/js/slick.min.js')}}"></script>
    <script src="{{asset('site/js/intl-tel-input.js')}}"></script>
    <script src="{{asset('site/js/custom.js')}}"></script>
</head>
