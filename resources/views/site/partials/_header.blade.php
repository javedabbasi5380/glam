<header id="header-com">
    <nav class="navbar navbar-expand-lg navbar-light back-header">
        <div class="container">
            <a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('site/images/logo.png')}}" alt=""></a>
            <button class="navbar-toggler custom-mobile-menu" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon navbar-toggler-icon-custom"></span>
            </button>
            <div class="collapse navbar-collapse custom-navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                </ul>
                <div class="d-flex">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item custom-header-link-li">
                            <a class="nav-link text-white custom-header-link custom-link-active" href="{{route('home')}}">Home</a>
                        </li>
                        <li class="nav-item custom-header-link-li">
                            <a class="nav-link text-white custom-header-link" href="{{route('business_setup')}}">Set Up My Business</a>
                        </li>
                        <li class="nav-item custom-header-link-li">
                            <a class="nav-link text-white custom-header-link" href="{{route('signup')}}">Sign Up</a>
                        </li>
                        <li class="nav-item custom-header-link-li">
                            <a class="nav-link text-white custom-header-link" href="{{route('login')}}">Log In</a>
                        </li>
                        <li class="nav-item custom-header-link-li">
                            <a class="nav-link text-white custom-header-link" href="{{route('help')}}">Help</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
