@section('title','Shops')
@include('site.partials._head')
<body>
@include('site.partials._header')
<div class="container-fluid p-0 shop-back back-img-str ">
    <div class="container">
        <h2 class="page-titles">Shops</h2>
    </div>
</div>

<!-- search shops -->
<div class="container mt-60 mb-60">
    <div class="row">
        <div class="col-md-12">
            <div class="show-serach-area">
                <input type="search" name="" placeholder="Search For Shop" id="">
                <i class="fad fa-search"></i>
            </div>

        </div>
    </div>
</div>

<!-- Recommended Shop For You -->
<div class="container recommended-container mt-60 mb-60">
    <div class="row recommended-row">
        <div class="col-md-12">
            <div class="service-explore">
                <p class="mb-0">Explore Awesome Services</p>
                <h4>Recommended Shop For You</h4>
            </div>
        </div>
    </div>

    <div class="row service-products">
        <!-- product 1 -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>

        <!-- product 1 -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-2.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>

        <!-- product 1 -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-3.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>

        <!-- product 1 -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-4.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
    </div>

    <div class="row ">
        <div class="col-md-12 text-center">
            <button class="explore-more">Explore Other Shop</button>
        </div>
    </div>
</div>

<!-- Sneakers Never Run Out Of Style Get 50% Off Sitewide -->
<div class="container-fluid p-0 back-img-str mb-60 sneakers-back">
    <div class="sneaker-inner">
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-white text-lg-start text-sm-center">Sneakers Never Run Out Of Style <br>
                    Get 50% Off Sitewide</h2>
            </div>
            <div class="col-md-6 start-service-box">
                <button class="start-services-btns">Start Services</button>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="service-explore text-center">
                <p class="mb-0">Straight From Shop</p>
                <h4>New Brows by shop</h4>
            </div>
        </div>
    </div>

    <div class="row mt-5 mb-60">
        <!-- nested columbn -->
        <div class="col-md-6">
            <div class="row">
                <!-- nested single -->
                <div class="col-6 mb-4">
                    <div class="single-product-divs">
                        <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                        <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                        <h6 class="mt-2">Saloon 01</h6>
                        <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
                    </div>
                </div>
                <!-- nested single -->
                <div class="col-6 mb-4">
                    <div class="single-product-divs">
                        <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                        <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                        <h6 class="mt-2">Saloon 01</h6>
                        <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
                    </div>
                </div>

                <!-- nested single -->
                <div class="col-6 mb-4">
                    <div class="single-product-divs">
                        <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                        <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                        <h6 class="mt-2">Saloon 01</h6>
                        <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
                    </div>
                </div>

                <!-- nested single -->
                <div class="col-6 mb-4">
                    <div class="single-product-divs">
                        <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                        <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                        <h6 class="mt-2">Saloon 01</h6>
                        <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
                    </div>
                </div>
            </div>

        </div>

        <!-- single column -->
        <div class="col-md-6">
            <div class="single-pro-column">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <h5>Saloon 01</h5>
                <img src="{{asset('site/images/single-pro.png')}}" alt="">
                <h6 class="mt-4"><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>

        </div>
    </div>
    <div class="row mb-60 ">
        <div class="col-md-12 text-center">
            <button class="explore-more">Explore Other Shop</button>
        </div>
    </div>
</div>

<!-- banner-2 -->
<div class="container-fluid p-0 back-img-str mb-60 sneakers-2">
    <div class="sneaker-inner">
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-white text-lg-start text-sm-center">Sneakers Never Run Out Of Style <br>
                    Get 50% Off Sitewide</h2>
            </div>
            <div class="col-md-6 start-service-box">
                <button class="start-services-btns">Start Services</button>
            </div>
        </div>
    </div>
</div>

<!-- Trending This Week -->
<div class="container mb-5">
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="service-explore text-center">
                <p class="mb-0">Straight From Shop</p>
                <h4>New Brows by shop</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- column start -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
        <!-- column end -->

        <!-- column start -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
        <!-- column end -->


        <!-- column start -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
        <!-- column end -->


        <!-- column start -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
        <!-- column end -->
        <!-- column start -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
        <!-- column end -->
        <!-- column start -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
        <!-- column end -->
        <!-- column start -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
        <!-- column end -->
        <!-- column start -->
        <div class="col-md-6 col-lg-3 mb-4">
            <div class="single-product-divs">
                <span class="rating-box"><i class="fas fa-star me-1"></i> 2.5</span>
                <img src="{{asset('site/images/pro-1.png')}}" alt="sda">
                <h6 class="mt-2">Saloon 01</h6>
                <h6><i class="fas fa-map-marker-alt me-2"></i>USA</h6>
            </div>
        </div>
        <!-- column end -->
    </div>
</div>
@include('site.partials._footer')
</body>
</html>
