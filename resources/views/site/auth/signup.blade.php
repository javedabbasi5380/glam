@section('title','Signup')
@include('site.partials._head')
<body>
@include('site.partials._header')
<div class="container-fluid p-0 sign-up-back back-img-str" ></div>

<div class="container mt-60">

    <div class="signup-box">
        <div class="row">
            <div class=col-md-12>
                <div class="create-an-amonut">
                    <div class="create-text-center">
                        <h3 class="mb-4">Create an account and <br>
                            discover the benefits</h3>
                        <p class="text-light-gray font-14 mb-4">Lorem Ipsum is simply dummy text of the printing
                            and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    </div>

                    <div class="signup-form">
                        <form action="" method="POST">
                            @csrf
                            <div class="single-input">
                                <label class="top-lable-design" for="">First Name</label>
                                <input type="text" placeholder="First Name" class="top-lable-design-input" name="" id="">
                            </div>

                            <div class="single-input">
                                <label class="top-lable-design" for="">Last Name</label>
                                <input type="text" placeholder="Last Name" class="top-lable-design-input" name="" id="">
                            </div>

                            <div class="single-input">
                                <label class="top-lable-design" for="">Email</label>
                                <input type="email" placeholder="Email" class="top-lable-design-input" name="" id="">
                            </div>

                            <div class="single-input">
                                <label class="top-lable-design" for="">Password</label>
                                <input type="password" placeholder="Password" class="top-lable-design-input" name="" id="">
                            </div>

                            <div class="single-input text-center mt-5">

                                <label class="top-lable-design-check" for="agree-to-signup"><input type="checkbox" name="" id="agree-to-signup">   I agree to the Google Terms of Service and Privacy Policy</label>
                            </div>
                            <div class="signup-buttons">
                                <button class="signup-btns btn btn-danger d-block w-100 p-3">Sign up</button>
                            </div>
                        </form>
                        <div class="are-you-member mt-5 text-center mb-60">
                            <a href="{{route('login')}}" class=" text-decoration-none text-black">Are you already a member?</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@include('site.partials._footer')

</body>
</html>
