@section('title','Seller Signup')
@include('site.partials._head')
<body>
@include('site.partials._header')
<div class="modal fade" id="location-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="location-modals">
                    <form action="">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="font-popine-bold business-text">Where's your business located?</h2>
                                <label class="top-lable-design-check font-14" for="agree-to-signup"> <input type="checkbox" class="me-2" name="" id="notification-alert">Get text notifications for new bookings, reschedules & more. </label>
                            </div>
                            <div class="col-md-12">
                                <div class="mt-3">
                                    <label class="top-lable-design" for="">Street address</label>
                                    <input type="text" placeholder="Street address" class="top-lable-design-input" name="" id="">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="mt-3">
                                    <label class="top-lable-design" for="">Suite, Apt, etc. (optional)</label>
                                    <input type="text" placeholder="Suite, Apt, etc" class="top-lable-design-input" name="" id="">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="mt-3">
                                    <label class="top-lable-design" for="">City</label>
                                    <input type="text" placeholder="City" class="top-lable-design-input" name="" id="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mt-3">
                                    <label class="top-lable-design" for="">State</label>
                                    <input type="text" placeholder="State" class="top-lable-design-input" name="" id="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mt-3">
                                    <label class="top-lable-design" for="">Zip</label>
                                    <input type="text" placeholder="Zip" class="top-lable-design-input" name="" id="">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mt-3 text-center">
                                    <button type="button" class="save-and-continues close-location-modal">SAVE AND CONTINUE</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="profile-modal" tabindex="-1" aria-labelledby="profileModalLabel" aria-hidden="true">
    <div class="modal-dialog build-client-profile">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container build-connection-modal ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <h3>Build a connection with new clients</h3>
                                <p>Put a face to your brand and add a business name.</p>
                            </div>
                        </div>
                    </div>
                    <form action="">
                        <div class="form-connections">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="">
                                        <div class="circle">
                                            <!-- User Profile Image -->
                                            <img class=" profile-pic-22" src="{{asset('site/images/s-1.png')}}">
                                            <div class="p-image">
                                                <i class="fas fa-plus-circle upload-button"></i>
                                                <input class="file-upload" type="file" accept="image/*" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <p class="text-center mt-3">(Optional)</p>
                                        <p class="font-14">Clients will see your full name & business name in our search directory and on your profile.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mt-3">
                                        <label class="top-lable-design" for="">First Name</label>
                                        <input type="text" placeholder="First Name" class="top-lable-design-input" name="" id="">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mt-3">
                                        <label class="top-lable-design" for="">Last Name</label>
                                        <input type="text" placeholder="Last Name" class="top-lable-design-input" name="" id="">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="mt-3">
                                        <label class="top-lable-design" for="">Business name</label>
                                        <input type="text" placeholder="Business name" class="top-lable-design-input" name="" id="">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="button" class="save-and-continues open-locate-modal">SAVE AND CONTINUE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid px-0 accout-setup">
    <div class="container">
        <div class="account-form">
            <span class="font-14 text-danger">LIST YOUR BUSINESS</span>
            <h4 class="font-popine-bold">Grow your beauty or barber business risk free</h4>
            <form  class="account-setup-form" action="">
                <div class="single-input">
                    <label class="top-lable-design" for="">First Name</label>
                    <input type="text" placeholder="First Name" class="top-lable-design-input" name="" id="">
                </div>

                <div class="single-input">
                    <label class="top-lable-design" for="">Last Name</label>
                    <input type="text" placeholder="Last Name" class="top-lable-design-input" name="" id="">
                </div>

                <div class="single-input">
                    <label class="top-lable-design" for="">Email</label>
                    <input type="email" placeholder="Email" class="top-lable-design-input" name="" id="">
                </div>

                <div class="single-input">
                    <label class="top-lable-design" for="">Password</label>
                    <input type="password" placeholder="Password" class="top-lable-design-input" name="" id="">
                </div>
                <div class="single-input-mobile">
                    <label class="top-lable-design-mobile" for="">Phone number</label>
                    <input id="phone" name="phone" class="top-lable-design-input-mobile" type="tel">
                </div>
                <div class="single-input dropdown-box-payments">
                    <label class="top-lable-design" for="">Where did you hear about us?</label>
                    <select name="" class="payments-months w-100" id="">
                        <option value="value">value</option>
                        <option value="value">value</option>
                        <option value="value">value</option>
                        <option value="value">value</option>
                        <option value="value">value</option>

                    </select>
                </div>


                <div class="single-input text-center mt-5">

                    <label class="top-lable-design-check" for="agree-to-signup"> <i class="fas fa-bell me-2"></i>  Get text notifications for new bookings, reschedules & more. <input type="checkbox" class="ms-2" name="" id="agree-to-signup"></label>
                </div>
                <div class="signup-buttons">
                    <button class="signup-btns btn btn-danger d-block w-100 open-profile-modal p-3">Sign up</button>
                </div>
            </form>
            <div class="are-you-member mt-5 text-center mb-60">
                <a href="login.php" class=" text-decoration-none text-black">Are you already a member? <span class="text-danger">Login</span> </a>
            </div>
        </div>

    </div>
</div>

@include('site.partials._footer')
</body>
</html>
