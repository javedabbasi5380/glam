@section('title','Login')
@include('site.partials._head')
<body>
@include('site.partials._header')
<div class="container-fluid p-0 sign-up-back back-img-str"></div>

<div class="container mt-60">

    <div class="signup-box">
        <div class="row">
            <div class=col-md-12>
                <div class="create-an-amonut">
                    <div class="create-text-center">

                        <h3 class="mb-4">Log in</h3>
                        <p class="text-light-gray font-14 mb-4">Lorem Ipsum is simply dummy text of the printing
                            and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    </div>

                    <div class="signup-form">
                        <form action="{{route('authenticate')}}" method="POST">
                            @csrf
                            <div class="single-input">
                                <label class="top-lable-design" for="">Email</label>
                                <input type="email" placeholder="Email" class="top-lable-design-input" name="email" id="">
                            </div>

                            <div class="single-input">
                                <label class="top-lable-design" for="">Password</label>
                                <input type="password" placeholder="Password" class="top-lable-design-input" name="password" id="">
                            </div>

                            <div class="single-input-keep-signin mt-5">
                                <label class="top-lable-design-check float-start" for="agree-to-signup-1"><input type="checkbox" name="" id="agree-to-signup-1"> Keep me signed in</label>
                                <div class="float-end">
                                    <a href="{{route('forgot')}}" class=" text-decoration-none text-black"> Forgot password?</a>
                                </div>
                            </div>
                            <div class="social-icons">
                                <button class="facebook-btns"> <img src="{{asset('site/images/icons/fb.png')}}" alt=""> Facebook</button>
                                <button class="gmail-btns"> <img src="{{asset('site/images/icons/gmail.png')}}" alt="">  Gmail</button>
                            </div>
                            <div class="login-buttons">
                                <button type="submit" class="signup-btns btn btn-danger d-block w-100 p-3">Login</button>
                            </div>
                        </form>
                        <div class="are-you-member mt-5 text-center mb-60">
                            <a href="{{route('signup')}}" class=" text-decoration-none text-black">Not a member yet? <b>Sign up</b></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@include('site.partials._footer')
</body>
</html>
