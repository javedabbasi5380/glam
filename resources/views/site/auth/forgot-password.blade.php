@section('title','Forgot Password')
@include('site.partials._head')
<body>
@include('site.partials._header')
<div class="container-fluid p-0 forget-back back-img-str"></div>

<div class="container mt-60">

    <div class="signup-box">
        <div class="row">
            <div class=col-md-12>
                <div class="create-an-amonut">
                    <div class="create-text-center">
                        <h3 class="mb-4">Forgot your password?</h3>
                        <p class="text-light-gray font-14 mb-4">Lorem Ipsum is simply dummy text of the printing
                            and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    </div>

                    <div class="signup-form mb-60">
                        <form action="">
                            <div class="single-input">
                                <label class="top-lable-design" for="">Email</label>
                                <input type="email" placeholder="Email" class="top-lable-design-input" name="" id="">
                            </div>
                            <div class="single-input-mobile">
                                <hr class="or-dividers">
                            </div>
                            <div class="single-input-mobile">
                                <label class="top-lable-design-mobile" for="">Phone number</label>
                                <input id="phone" name="phone" class="top-lable-design-input-mobile" type="tel">
                            </div>
                            <div class="login-buttons">
                                <button class="signup-btns btn btn-danger d-block w-100 p-3">Reset password</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@include('site.partials._footer')
<script>
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
        utilsScript: "site/js/time-utils.js",
    });
</script>
</body>
</html>
