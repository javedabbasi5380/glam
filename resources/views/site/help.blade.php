@section('title','Help')
@include('site.partials._head')
<body>
@include('site.partials._header')
<div class="container-fluid p-0 help-back back-img-str">
    <div class="container">
        <h2 class="font-bold text-white">Help</h2>
    </div>
</div>
<div class="container mt-60  mb-60">
    <div class="row">
        <div class="col-md-12">
            <div class="search-for-help">
                <h2>How can we help you today?</h2>
                <div class="search-help-box-form mt-5">
                    <input type="search" placeholder="Enter your search term here..." name="" id="">
                    <button><i class="fal fa-search"></i></button>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="container knowledge-container mb-60">
    <h2>Knowledge base</h2>
    <div class="knowledge-base-hrs">
        <p>For Clients</p>
        <hr>
    </div>
    <div class="getting-records-helps">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="reacord-box">
                    <h5>Getting Started on Glam (5)</h5>
                    <ul>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                    </ul>
                </div>

            </div>

            <div class="col-md-6 mb-4">
                <div class="reacord-box">
                    <h5>Getting Started on Glam (5)</h5>
                    <ul>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                    </ul>
                </div>

            </div>


            <div class="col-md-6 mb-4">
                <div class="reacord-box">
                    <h5>Getting Started on Glam (5)</h5>
                    <ul>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                    </ul>
                </div>

            </div>


            <div class="col-md-6 mb-4">
                <div class="reacord-box">
                    <h5>Getting Started on Glam (5)</h5>
                    <ul>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <div class="knowledge-base-hrs">
        <p>For Professionals</p>
        <hr>
    </div>
    <div class="getting-records-helps">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="reacord-box">
                    <h5>Getting Started on Glam (5)</h5>
                    <ul>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                    </ul>
                </div>

            </div>

            <div class="col-md-6 mb-4">
                <div class="reacord-box">
                    <h5>Getting Started on Glam (5)</h5>
                    <ul>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                    </ul>
                </div>

            </div>


            <div class="col-md-6 mb-4">
                <div class="reacord-box">
                    <h5>Getting Started on Glam (5)</h5>
                    <ul>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                    </ul>
                </div>

            </div>


            <div class="col-md-6 mb-4">
                <div class="reacord-box">
                    <h5>Getting Started on Glam (5)</h5>
                    <ul>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                        <li><a href="#">Lorem Ipsum is simply dummy. </a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

</div>
@include('site.partials._footer')
</body>
</html>
