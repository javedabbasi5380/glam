@section('title','Business Setup')
@include('site.partials._head')
<body>
@include('site.partials._header')
<div class="container-fluid setup-business back-img-str">
    <div class="container">
        <div class="business-setup-box">
            <h6 class="this-years text-danger">This is the year you</h6>
            <h1 class="text-danger text-doubles">Double</h1>
            <h5 class="text-6D6967 text-revinues">your revenue</h5>
            <p class="text-6D6967 font-popine-bold">The #1 beauty & grooming marketplace for getting exposure to new clients</p>
            <div class="mt-4 mb-4">
                <a href="{{route('seller_signup')}}" class="getting-start-business">Get Started</a>
            </div>
            <p class="mt-4 text-white">The #1 beauty & grooming marketplace for getting exposure to new clients</p>

        </div>
    </div>

</div>

<div class="container-fluid px-0 mt-60">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="font-popine-bold">Why Glam?</h3>
                <p>We don’t just help you run your business, we help you grow it. Earn more money, get exposure to millions of clients, and keep your regulars coming back for zero monthly subscription cost.</p>
            </div>
            <div class="col-md-12">
                <div class="position-relative testimonial-blocks">
                    <p class="testimonial-texts">I get so many clients who say they found me through Glam. My business would not be anywhere close to where it is without it.</p>
                    <img src="{{asset('site/images/earn-mony-img.png')}}" alt="">
                    <h6 class="name-of-businesses">--- Katie Allen, Makeup Artist</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid px-0 mt-60 mb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">Earn More Money</h3>
            </div>
        </div>
        <div class="row mt-60">
            <div class="col-md-6">
                <div class="earn-more-box-1">
                    <div class="img-box-setup">
                        <img src="{{asset('site/images/icons/card-icons.png')}}" alt="">
                    </div>
                    <h4 class="font-poppins-medium">Touch-Free Payments</h4>
                    <p class="mt-3">Charge clients seamlessly through the app and get paid instantly. No unreliable swipers or need for clients to pull out their wallet.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="earn-more-box-2">
                    <div class="img-box-setup">
                        <img src="{{asset('site/images/icons/calander.png')}}" alt="">
                    </div>
                    <h4 class="font-poppins-medium">Keep your schedule full</h4>
                    <p  class="mt-3">When you have last-minute cancellations, we’ll reach out to clients who are most likely to want those spots and give them early access to new opening.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@include('site.partials._footer')
</body>
</html>
