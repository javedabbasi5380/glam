@section('title','Homepage')
@include('site.partials._head')
<body>
@include('site.partials._header')
<div class="home-back-img container-fluid p-0">
    <div class="find-and-book align-self-center">
        <div class="home-social-div">
            <ul>
                <li class="home-social-div-link"><a href="#"><i class="fas fa-globe-americas text-danger"></i></a></li>
                <li class="home-social-div-link"><a href="#"><i class="fab fa-facebook-f text-danger"></i></a></li>
                <li class="home-social-div-link"><a href="#"><i class="fab fa-twitter text-danger"></i></a></li>
                <li class="home-social-div-link"><a href="#"><i class="fab fa-instagram text-danger"></i></a></li>
            </ul>
        </div>
        <h1 class="text-white mb-4">
            Find and book beauty <br>
            specialists in your area.
        </h1>
        <div class="find-and-book-inputs">
            <form action="" class="find-and-book-inputs-form">
                <div class="inputs-book me-lg-2 me-md-0">
                    <i class="fal fa-search text-danger"></i>
                    <input type="text" placeholder="Search Stylish or Saloon" class="input-with-icon text-danger">

                </div>
                <div class="inputs-book">
                    <input type="text" placeholder="City or Zip Code" class="input-with-icon text-danger">
                    <i class="fas fa-map-marker-alt text-danger"></i>
                </div>
                <div class="">
                    <button type="submit" class="position-relative search-submit bg-danger"><i class="fal fa-search"></i></button>
                </div>
            </form>
        </div>
        <p class="text-white are-you-better"><i class="fal fa-arrow-circle-right text-white me-2"></i>Are you a barber or a beauty expert?</p>
    </div>

</div>

<div class="container mt-60">
    <div class="row">
        <div class="col-md-4 mb-3">
            <div class="make-up-box">
                <img src="{{asset('site/images/mack-1.png')}}" alt="">
                <div class="make-up-inner">
                    <h3 class="text-white">MAKE UP</h3>
                    <p class="text-white">Eleifend lobortis bibendum</p>
                    <button class="btn custon-by-mackups-btn">$60*/M</button>
                </div>

            </div>

        </div>
        <div class="col-md-4  mb-3">
            <div class="make-up-box">
                <img src="{{asset('site/images/mack-2.png')}}" alt="">
                <div class="make-up-inner">
                    <h3 class="text-white">MAKE UP</h3>
                    <p class="text-white">Eleifend lobortis bibendum</p>
                    <button class="btn custon-by-mackups-btn">$60*/M</button>
                </div>
            </div>
        </div>
        <div class="col-md-4  mb-3">
            <div class="make-up-box">
                <img src="{{asset('site/images/mack-3.png')}}" alt="">
                <div class="make-up-inner">
                    <h3 class="text-white">MBEAUTY SERVICES</h3>
                    <p class="text-white">Eleifend lobortis bibendum</p>
                    <button class="btn custon-by-mackups-btn">$120*/M</button>
                </div>
            </div>
        </div>


    </div>
</div>

<!-- Find top pros by services -->
<div class="container-fluid p-0 mt-60 back-light-blcak">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="find-top">
                    <h2 class="text-white">Find top pros by services</h2>
                    <hr>
                    <p class="text-white text-center">I HAVE NO REGRETS ABOUT LAUNCHING SALON. FOR THE LIFE OF ME, I CAN'T IMAGINE DOING ANYTHING ELSE. Has Been At The Heart Of Our Working Culture At @Envato, And This Means Continuing</p>

                </div>

            </div>
        </div>
    </div>

</div>
<!-- Find top pros by services geelry -->
<div class="container-fluid p-0">
    <div class="row g-0">
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="find-top-inner-box">
                <img src="{{asset('site/images/top-find-2.png')}}" alt="">
                <div class="find-top-img-text">
                    <h5 class="text-white">Makeup</h5>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="find-top-inner-box">
                <img src="{{asset('site/images/top-find-5.png')}}" alt="">
                <div class="find-top-img-text">
                    <h5 class="text-white">Pedicure</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="find-top-inner-box">
                <img src="{{asset('site/images/top-find-1.png')}}" alt="">
                <div class="find-top-img-text">
                    <h5 class="text-white">Manicure</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="find-top-inner-box">
                <img src="{{asset('site/images/top-find-3.png')}}" alt="">
                <div class="find-top-img-text">
                    <h5 class="text-white">Hair Cut</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="find-top-inner-box">
                <img src="{{asset('site/images/top-find-4.png')}}" alt="">
                <div class="find-top-img-text">
                    <h5 class="text-white">Massage</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="find-top-inner-box">
                <img src="{{asset('site/images/top-find-6.png')}}" alt="">
                <div class="find-top-img-text">
                    <h5 class="text-white">Face Waxing</h5>
                </div>
            </div>
        </div>


    </div>
</div>

<!-- Bring Your Natural Beauty -->
<div class="container-fluid p-0 mt-60 mb-60">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="natural-buety-box-page">
                    <div class="natural-buety-box">
                        <p class="text-info mb-0">ABOUT US</p>
                        <h2>Bring Your Natural Beauty</h2>
                        <hr class="hr-left">
                        <h6 class="mt-4 mb-4">We Belong to Something Beautiful</h6>
                        <p class="text-light-gray">Our company announced a new tagline and manifesto, to reinforce our dedication to fostering belonging among all clients and employees and to publicly strive for a more inclusive vision for retail in the Americas.</p>
                        <div class="d-flex  natural-buety-icons">
                            <p> <span><img src="{{asset('site/images/icons/nature.png')}}" alt=""></span> Natural Skincare</p>
                            <p><span><img src="{{asset('site/images/icons/envole.png')}}" alt=""></span>  Evolve Heathier</p>
                            <p>  <span> <img src="{{asset('site/images/icons/frinedly.png')}}" alt=""></span>Eco-Friendly</p>
                        </div>
                        <div class="mt-5">
                            <a href="#" class="btn btn-danger">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 mt-50-767">
                <div class="nature-img">
                    <img src="{{asset('site/images/natural-buety.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>


</div>

<!-- We love fashion -->
<div class="container-fluid p-0 mt-60 fasion-back-img">
    <div class="container">
        <div class="row " >
            <div class="col-md-12">
                <div class="position-relative">
                    <div class="we-love-text-box">
                        <h5 class="we-love-text text-white">We love</h5>
                        <h1 class="text-danger">Makeup</h1>
                        <p class="text-white">Donec id nunc quis nunc tincidunt hendrerit. Aliquam facilisis laoreet mauris nec tincidunt. Phasellus tempor laoreet accumsan.</p>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Want to be a Glam Pro? -->
<div class="container mt-60 mb-60">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3 class="text-light-gray pb-4">Want to be a <span class="text-danger">Glam</span>  Pro?</h3>
            <p  class="text-light-gray">Our mission has always been to give talented Pros the tools they need to drive their business and bring home the bacon while empowering them to do what they do best: create looks, inspire confidence and make their clients feel fierce AF.</p>
            <div class="mt-4">
                <a href="#" class="btn btn-danger">SET UP YOUR BUINESS</a>
            </div>
        </div>
    </div>
</div>

<!-- Contact Us -->
<div class="container-fluid back-light-blcak back-img-str">
    <div class="row ">
        <div class="col-md-6">
            <div class="contact-form">
                <h1 class="text-white">Contact Us</h1>
                <hr>


                <form action="">
                    <select class="form-inputs-contacts height-40" name="" id="">
                        <option value="value">value</option>
                        <option value="value">value</option>
                        <option value="value">value</option>
                    </select>
                    <input placeholder="Email" class="form-inputs-contacts height-40" type="email" name="" id="">
                    <textarea class="form-inputs-contacts" placeholder="Write Your Message Here..."  name="" id="" cols="30" rows="4"></textarea>
                    <button class="btn btn-danger d-block w-100">Submit</button>
                </form>
                <p class="text-white font-14 mt-4">glam@gmail.Com | +00 1111 22 12345</p>
                <div class="contact-form-icon">
                    <ul>
                        <li><a href=""><i class="fab fa-facebook-f  text-info"></i></a></li>
                        <li><a href=""><i class="fab fa-linkedin-in text-info"></i></a></li>
                        <li><a href=""><i class="fab fa-twitter text-info"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 pe-0">
            <div class="contact-back-imgs">
                <!-- <img src="images/contact-back.png" alt=""> -->
            </div>
        </div>
    </div>
</div>
@include('site.partials._footer')
</body>
</html>
