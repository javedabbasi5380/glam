@include('admin.partials._head')
<body class="nk-body bg-lighter npc-general has-sidebar ">
<div class="nk-app-root">
    <div class="nk-main ">
        @include('admin.partials._sidebar')
        <div class="nk-wrap ">
            @include('admin.partials._header')
            <div class="nk-content ">
                <div class="container-fluid">
                    <div class="nk-content-inner">
                        <div class="nk-content-body">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.partials._scripts')
</body>
</html>
