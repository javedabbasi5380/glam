@include('admin.partials._head')
<body class="nk-body bg-white npc-general pg-auth">
<div class="nk-app-root">
    <div class="nk-main ">
        <div class="nk-wrap nk-wrap-nosidebar">
            <div class="nk-content ">
                @yield('content')
                @include('admin.partials._footer-page')
            </div>
        </div>
    </div>
</div>
@include('admin.partials._scripts')
</body>
</html>
