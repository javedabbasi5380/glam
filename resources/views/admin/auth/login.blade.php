@extends('admin.layouts.page')
@section('title','Login to Admin Panel')
@section('content')
    <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
        <div class="brand-logo pb-4 text-center">
            <a href="#" class="logo-link">
                <img class="logo-light logo-img logo-img-lg" src="{{asset('backend/images/logo.png')}}" srcset="{{asset('backend/images/logo.png')}}" alt="logo">
                <img class="logo-dark logo-img logo-img-lg" src="{{asset('backend/images/logo.png')}}" srcset="{{asset('backend/images/logo.png')}}" alt="logo-dark">
            </a>
        </div>

        @include('admin.partials._show-error-message')

        <div class="card card-bordered">
            <div class="card-inner card-inner-lg">
                <div class="nk-block-head">
                    <div class="nk-block-head-content">
                        <h4 class="nk-block-title">Sign-In</h4>
                        <div class="nk-block-des">
                            <p>Access the Glam admin panel using your email and password</p>
                        </div>
                    </div>
                </div>
                <form action="{{route('admin.authenticate')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="email">Email</label>
                        </div>
                        <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="Enter your email address">
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <label class="form-label" for="password">Password</label>
                            <a class="link link-primary link-sm" href="#">Forgot Password?</a>
                        </div>
                        <div class="form-control-wrap">
                            <a href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                            </a>
                            <input type="password" name="password" class="form-control form-control-lg" id="password" placeholder="Enter your password">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
