@extends('admin.layouts.dashboard')
@section('title','Edit User')
@section('content')
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">Edit User</h3>
                <div class="nk-block-des text-soft">
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="card card-bordered">
            <div class="card-inner">
                @include('admin.partials._show-validation-errors')

                <form action="{{route('admin.users.update',$user->id)}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="row g-4">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-label" for="first_name">First name</label>
                                <div class="form-control-wrap">
                                    <input type="text" name="first_name" class="form-control form-control-lg" id="first_name" value="{{$user->first_name}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-label" for="last_name">Last name</label>
                                <div class="form-control-wrap">
                                    <input type="text" name="last_name" class="form-control form-control-lg" id="last_name" value="{{$user->last_name}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-label" for="email">Email address</label>
                                <div class="form-control-wrap">
                                    <input type="email" name="email" class="form-control form-control-lg" id="email" value="{{$user->email}}" data-rule-email="true" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label" for="mobile_number">Mobile No</label>
                                <div class="form-control-wrap">
                                    <input type="text" name="mobile_number" class="form-control form-control-lg" id="mobile_number" value="{{$user->mobile_number}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label" for="role">Select Role</label>
                                <div class="form-control-wrap">
                                    <select class="form-select update_role_select" name="role" id="role" data-ui="lg" required>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" {{($user->role_id == $role->id) ? "selected":''}}>{{ucfirst($role->name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-lg btn-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- .nk-block -->
@endsection
