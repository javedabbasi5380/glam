@if(session('error'))
    <div class="alert alert-danger alert-icon alert-dismissible">
        <em class="icon ni ni-cross-circle"></em>
        {{session('error')}}
        <button class="close" data-dismiss="alert"></button>
    </div>
@endif
