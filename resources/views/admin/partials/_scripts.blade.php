<script src="{{asset('backend/js/bundle.js')}}"></script>
<script src="{{asset('backend/js/scripts.js')}}"></script>
<script src="{{asset('backend/js/charts/gd-default.js')}}"></script>
<script src="{{asset('backend/js/custom.js')}}"></script>
@stack('scripts')
