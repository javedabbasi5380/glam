@if(session('success'))
    <div class="alert alert-success alert-icon alert-dismissible">
        {{session('success')}}
        <button class="close" data-dismiss="alert"></button>
    </div>
@endif
