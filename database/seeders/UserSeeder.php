<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123'),
            'mobile_number' => '+12025550101',
            'role_id' => 1,
        ]);

        User::create([
            'first_name' => 'Sara',
            'last_name' => 'Doe',
            'email' => 'customer@customer.com',
            'password' => Hash::make('123'),
            'mobile_number' => '+12025550199',
            'role_id' => 2,
        ]);

        User::create([
            'first_name' => 'Jack',
            'last_name' => 'Doe',
            'email' => 'seller@seller.com',
            'password' => Hash::make('123'),
            'mobile_number' => '+12025550190',
            'role_id' => 3,
        ]);
    }
}
