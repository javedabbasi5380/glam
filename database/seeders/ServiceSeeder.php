<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::create([
           'title' => 'Knotless braids',
            'price' => '165',
            'duration' => '240',
            'shop_id' => 1,
        ]);

        Service::create([
            'title' => 'Blowout',
            'description' => 'Enjoy a wash, blow dry, and fresh-from-the-salon confidence.',
            'price' => '50',
            'duration' => '60',
            'shop_id' => 1,
        ]);

        Service::create([
            'title' => 'Dreadlocks',
            'price' => '65',
            'duration' => '105',
            'shop_id' => 1,
        ]);

        Service::create([
            'title' => 'Full Sew In',
            'price' => '130',
            'description' => 'With a full sew in, hair is braided down, wefts are sewn on from ear to ear, and a closure is added along the hairline.',
            'duration' => '120',
            'shop_id' => 1,
        ]);

    }
}
