<?php

namespace Database\Seeders;

use App\Models\Shop;
use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shop::create([
            'name' => 'Blunt Effects Co.',
            'street_address' => '38.9597081,-76.9954406',
            'city' => 'Hyattsville',
            'state' => 'Maryland',
            'zip' => '20781',
            'user_id' => 3,
        ]);
    }
}
