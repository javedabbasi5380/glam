<?php

namespace Database\Seeders;

use App\Models\OpeningHour;
use Illuminate\Database\Seeder;

class OpeningHourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OpeningHour::create([
            'shop_id' => 1,
            'day' => 'monday',
            'open_time' => '09:00:00',
            'close_time' => '18:00:00',
        ]);

        OpeningHour::create([
            'shop_id' => 1,
            'day' => 'tuesday',
            'open_time' => '09:00:00',
            'close_time' => '18:00:00',
        ]);

        OpeningHour::create([
            'shop_id' => 1,
            'day' => 'wednesday',
            'open_time' => '09:00:00',
            'close_time' => '18:00:00',
        ]);

        OpeningHour::create([
            'shop_id' => 1,
            'day' => 'thrusday',
            'open_time' => '09:00:00',
            'close_time' => '18:00:00',
        ]);

        OpeningHour::create([
            'shop_id' => 1,
            'day' => 'friday',
            'open_time' => '09:00:00',
            'close_time' => '18:00:00',
        ]);

        OpeningHour::create([
            'shop_id' => 1,
            'day' => 'saturday',
            'open_time' => '09:00:00',
            'close_time' => '18:00:00',
        ]);

        OpeningHour::create([
            'shop_id' => 1,
            'day' => 'sunday',
            'is_closed' => 1,
        ]);
    }
}
