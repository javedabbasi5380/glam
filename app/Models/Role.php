<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    // one to many relationship with users where 1 role can have many users and 1 user can have 1 role
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
