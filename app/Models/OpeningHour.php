<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpeningHour extends Model
{
    use HasFactory;

    protected $fillable = [
        'shop_id',
        'open_time',
        'close_time',
        'is_closed',
    ];


    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
