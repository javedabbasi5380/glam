<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    public function index()
    {
        $services = Service::with(['shop','shop.user'])->paginate(10);
        return  view('admin.services.index',['services' => $services]);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Service $service)
    {
        //
    }


    public function edit(Service $service)
    {
        //
    }


    public function update(Request $request, Service $service)
    {
        //
    }


    public function destroy($id)
    {
        $service = Service::where('id',$id)->first();
        $service->delete();
    }
}
