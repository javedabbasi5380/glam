<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        return view('site.index');
    }

    public function about()
    {
        return view('site.about');
    }

    public  function login()
    {
        return view('site.auth.login');
    }

    public function forgot()
    {
        return view('site.auth.forgot-password');
    }

    public function signup()
    {
        return view('site.auth.signup');
    }

    public function sellerSignup()
    {
        return view('site.auth.seller-signup');
    }

    public function help()
    {
        return view('site.help');
    }

    public function businessSetup()
    {
        return view('site.business-setup');
    }

    public function shops()
    {
        return view('site.shops');
    }
}
