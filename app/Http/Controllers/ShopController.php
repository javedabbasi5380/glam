<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{

    public function index()
    {
        $shops = Shop::with('user')->paginate(10);
        return view('admin.shops.index',['shops' => $shops]);
    }


    public function create()
    {
    }


    public function store(Request $request)
    {
    }


    public function show($id)
    {

    }


    public function edit($id)
    {

    }


    public function update(Request $request, $id)
    {

    }


    public function destroy($id)
    {
        $shop = Shop::where('id',$id)->first();
        if ($shop)
        {
            $shop->services()->delete();
            $shop->openingHours()->delete();
            $shop->delete();
            return response()->json(['success' => 'Shop deleted successfully']);
        }
        return response()->json(['error' => 'Something went wrong']);
    }
}
