<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index ()
    {
        $users = User::with(['role','shop'])->paginate(10);
        return view('admin.users.index',['users' => $users]);
    }

    public function show ($id)
    {
        $user = User::where('id',$id)->with(['role','shop'])->first();
        return view('admin.users.show',['user' => $user]);
    }
}
