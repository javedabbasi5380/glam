<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function userAuthenticate (Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            if ($user->hasRole('buyer')||$user->hasRole('seller'))
            {
                $request->session()->regenerate();
                $user->last_login = now();
                $user->save();
                return redirect()->route('home');
            }
        }

        return back()->with(['error' => 'Invalid email address or password']);
    }

    public function adminLogin ()
    {
        return view('admin.auth.login');
    }

    public function adminAuthenticate (Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            if ($user->hasRole('admin'))
            {
                $request->session()->regenerate();
                $user->last_login = now();
                $user->save();
                return redirect()->route('admin.dashboard');
            }
        }

        return back()->with(['error' => 'Invalid email address or password']);
    }

    public function logout (Request $request)
    {
        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
