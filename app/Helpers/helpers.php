<?php


if (!function_exists('getFirstLetterOfEveryWord')) {
     function getFirstLetterOfEveryWord($str) {
        $words = preg_split("/(\s|\-|\.)/", $str);
        $word = '';
        $acronym = '';
        foreach($words as $w) {
            $acronym .= substr($w,0,1);
        }
        $word = $word . $acronym ;
        return $word;
    }
}

