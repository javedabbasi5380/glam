<?php

use App\Http\Controllers\Admin\AdminDashboardController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\Site\SiteController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Site Routes
Route::get('/',[SiteController::class,'index'])->name('home');
Route::get('/about',[SiteController::class,'about'])->name('about');
Route::get('/help',[SiteController::class,'help'])->name('help');
Route::get('/business-setup',[SiteController::class,'businessSetup'])->name('business_setup');
Route::get('/shops',[SiteController::class,'shops'])->name('shops');

Route::middleware('guest')->group(function (){
    Route::get('/login',[SiteController::class,'login'])->name('login');
    Route::post('/authenticate',[AuthController::class,'userAuthenticate'])->name('authenticate');
    Route::get('/forgot',[SiteController::class,'forgot'])->name('forgot');
    Route::get('/signup',[SiteController::class,'signup'])->name('signup');
    Route::get('/seller-setup',[SiteController::class,'sellerSignup'])->name('seller_signup');
});

Route::get('/login',function (){
    return view('site.auth.login');
})->middleware('guest')->name('login');

// Admin Routes
Route::prefix('admin')->name('admin.')->group(function (){

    // Admin Guest Middleware Group
    Route::middleware('guest')->group(function (){

        Route::get('/login',[AuthController::class,'adminLogin'])->name('login');
        Route::post('/authenticate',[AuthController::class,'adminAuthenticate'])->name('authenticate');
    });

    // Admin Protected Middleware Group
    Route::middleware(['auth','role:admin'])->group(function (){

        Route::post('/logout',[AuthController::class,'logout'])->name('logout');
        Route::get('/dashboard',[AdminDashboardController::class,'index'])->name('dashboard');

        Route::resource('/users',UserController::class)->only(['index','show','destroy']);
        Route::resource('/shops',ShopController::class)->only(['index','show','destroy']);
        Route::resource('/services',ServiceController::class)->only(['index','show','destroy']);

    });
});
